unit WebPrinterImpl;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActiveX, AxCtrls, WebPrinterActiveX_TLB, StdVcl, StdCtrls, frxExportCSV,
  frxExportRTF, frxExportXLS, frxClass, frxExportPDF,WebFastReport,
  frxChart, frxRich, frxOLE, frxBarcode;

type
  TWebPrinter = class(TActiveForm, IWebPrinter)
    frxPDFExport1: TfrxPDFExport;
    frxXLSExport1: TfrxXLSExport;
    frxRTFExport1: TfrxRTFExport;
    frxCSVExport1: TfrxCSVExport;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxOLEObject1: TfrxOLEObject;
    frxRichObject1: TfrxRichObject;
    frxChartObject1: TfrxChartObject;
    procedure ActiveFormDestroy(Sender: TObject);
  private
    { Private declarations }
    URL:WideString;
    AppName:WideString;
    ReportName:WideString;
    Params:WideString;

    FEvents: IWebPrinterEvents;
    procedure ActivateEvent(Sender: TObject);
    procedure ClickEvent(Sender: TObject);
    procedure CreateEvent(Sender: TObject);
    procedure DblClickEvent(Sender: TObject);
    procedure DeactivateEvent(Sender: TObject);
    procedure DestroyEvent(Sender: TObject);
    procedure KeyPressEvent(Sender: TObject; var Key: Char);
    procedure PaintEvent(Sender: TObject);
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    function Get_Active: WordBool; safecall;
    function Get_AlignDisabled: WordBool; safecall;
    function Get_AutoScroll: WordBool; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_AxBorderStyle: TxActiveFormBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DropTarget: WordBool; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_HelpFile: WideString; safecall;
    function Get_KeyPreview: WordBool; safecall;
    function Get_PixelsPerInch: Integer; safecall;
    function Get_PrintScale: TxPrintScale; safecall;
    function Get_Scaled: WordBool; safecall;
    function Get_ScreenSnap: WordBool; safecall;
    function Get_SnapBuffer: Integer; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    procedure _Set_Font(var Value: IFontDisp); safecall;
    procedure Set_AutoScroll(Value: WordBool); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_AxBorderStyle(Value: TxActiveFormBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DropTarget(Value: WordBool); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(const Value: IFontDisp); safecall;
    procedure Set_HelpFile(const Value: WideString); safecall;
    procedure Set_KeyPreview(Value: WordBool); safecall;
    procedure Set_PixelsPerInch(Value: Integer); safecall;
    procedure Set_PrintScale(Value: TxPrintScale); safecall;
    procedure Set_Scaled(Value: WordBool); safecall;
    procedure Set_ScreenSnap(Value: WordBool); safecall;
    procedure Set_SnapBuffer(Value: Integer); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_URL: WideString; safecall;
    procedure Set_URL(const Value: WideString); safecall;
    function Get_AppName: WideString; safecall;
    procedure Set_AppName(const Value: WideString); safecall;
    function Get_ReportName: WideString; safecall;
    procedure Set_ReportName(const Value: WideString); safecall;
    procedure designReport; safecall;
    procedure printReport; safecall;
    procedure showReport; safecall;
    function Get_Params: WideString; safecall;
    procedure Set_Params(const Value: WideString); safecall;
  public
    { Public declarations }
    procedure Initialize; override;
  end;

implementation

uses ComObj, ComServ;

{$R *.DFM}

{ TWebPrinter }

procedure TWebPrinter.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  { Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_WebPrinterPage); }
end;

procedure TWebPrinter.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IWebPrinterEvents;
  inherited EventSinkChanged(EventSink);
end;

procedure TWebPrinter.Initialize;
begin
  inherited Initialize;
  OnActivate := ActivateEvent;
  OnClick := ClickEvent;
  OnCreate := CreateEvent;
  OnDblClick := DblClickEvent;
  OnDeactivate := DeactivateEvent;
  OnDestroy := DestroyEvent;
  OnKeyPress := KeyPressEvent;
  OnPaint := PaintEvent;
end;

function TWebPrinter.Get_Active: WordBool;
begin
  Result := Active;
end;

function TWebPrinter.Get_AlignDisabled: WordBool;
begin
  Result := AlignDisabled;
end;

function TWebPrinter.Get_AutoScroll: WordBool;
begin
  Result := AutoScroll;
end;

function TWebPrinter.Get_AutoSize: WordBool;
begin
  Result := AutoSize;
end;

function TWebPrinter.Get_AxBorderStyle: TxActiveFormBorderStyle;
begin
  Result := Ord(AxBorderStyle);
end;

function TWebPrinter.Get_Caption: WideString;
begin
  Result := WideString(Caption);
end;

function TWebPrinter.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(Color);
end;

function TWebPrinter.Get_DoubleBuffered: WordBool;
begin
  Result := DoubleBuffered;
end;

function TWebPrinter.Get_DropTarget: WordBool;
begin
  Result := DropTarget;
end;

function TWebPrinter.Get_Enabled: WordBool;
begin
  Result := Enabled;
end;

function TWebPrinter.Get_Font: IFontDisp;
begin
  GetOleFont(Font, Result);
end;

function TWebPrinter.Get_HelpFile: WideString;
begin
  Result := WideString(HelpFile);
end;

function TWebPrinter.Get_KeyPreview: WordBool;
begin
  Result := KeyPreview;
end;

function TWebPrinter.Get_PixelsPerInch: Integer;
begin
  Result := PixelsPerInch;
end;

function TWebPrinter.Get_PrintScale: TxPrintScale;
begin
  Result := Ord(PrintScale);
end;

function TWebPrinter.Get_Scaled: WordBool;
begin
  Result := Scaled;
end;

function TWebPrinter.Get_ScreenSnap: WordBool;
begin
  Result := ScreenSnap;
end;

function TWebPrinter.Get_SnapBuffer: Integer;
begin
  Result := SnapBuffer;
end;

function TWebPrinter.Get_Visible: WordBool;
begin
  Result := Visible;
end;

function TWebPrinter.Get_VisibleDockClientCount: Integer;
begin
  Result := VisibleDockClientCount;
end;

procedure TWebPrinter._Set_Font(var Value: IFontDisp);
begin
  SetOleFont(Font, Value);
end;

procedure TWebPrinter.ActivateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnActivate;
end;

procedure TWebPrinter.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TWebPrinter.CreateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnCreate;
end;

procedure TWebPrinter.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TWebPrinter.DeactivateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDeactivate;
end;

procedure TWebPrinter.DestroyEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDestroy;
end;

procedure TWebPrinter.KeyPressEvent(Sender: TObject; var Key: Char);
var
  TempKey: Smallint;
begin
  TempKey := Smallint(Key);
  if FEvents <> nil then FEvents.OnKeyPress(TempKey);
  Key := Char(TempKey);
end;

procedure TWebPrinter.PaintEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnPaint;
end;

procedure TWebPrinter.Set_AutoScroll(Value: WordBool);
begin
  AutoScroll := Value;
end;

procedure TWebPrinter.Set_AutoSize(Value: WordBool);
begin
  AutoSize := Value;
end;

procedure TWebPrinter.Set_AxBorderStyle(Value: TxActiveFormBorderStyle);
begin
  AxBorderStyle := TActiveFormBorderStyle(Value);
end;

procedure TWebPrinter.Set_Caption(const Value: WideString);
begin
  Caption := TCaption(Value);
end;

procedure TWebPrinter.Set_Color(Value: OLE_COLOR);
begin
  Color := TColor(Value);
end;

procedure TWebPrinter.Set_DoubleBuffered(Value: WordBool);
begin
  DoubleBuffered := Value;
end;

procedure TWebPrinter.Set_DropTarget(Value: WordBool);
begin
  DropTarget := Value;
end;

procedure TWebPrinter.Set_Enabled(Value: WordBool);
begin
  Enabled := Value;
end;

procedure TWebPrinter.Set_Font(const Value: IFontDisp);
begin
  SetOleFont(Font, Value);
end;

procedure TWebPrinter.Set_HelpFile(const Value: WideString);
begin
  HelpFile := String(Value);
end;

procedure TWebPrinter.Set_KeyPreview(Value: WordBool);
begin
  KeyPreview := Value;
end;

procedure TWebPrinter.Set_PixelsPerInch(Value: Integer);
begin
  PixelsPerInch := Value;
end;

procedure TWebPrinter.Set_PrintScale(Value: TxPrintScale);
begin
  PrintScale := TPrintScale(Value);
end;

procedure TWebPrinter.Set_Scaled(Value: WordBool);
begin
  Scaled := Value;
end;

procedure TWebPrinter.Set_ScreenSnap(Value: WordBool);
begin
  ScreenSnap := Value;
end;

procedure TWebPrinter.Set_SnapBuffer(Value: Integer);
begin
  SnapBuffer := Value;
end;

procedure TWebPrinter.Set_Visible(Value: WordBool);
begin
  Visible := Value;
end;
function TWebPrinter.Get_URL: WideString;
begin
  Result:=Self.URL;
end;

procedure TWebPrinter.Set_URL(const Value: WideString);
begin
  Self.URL:=Value;
end;

function TWebPrinter.Get_AppName: WideString;
begin
  Result:=Self.AppName;
end;

procedure TWebPrinter.Set_AppName(const Value: WideString);
begin
  Self.AppName:=Value;
end;

function TWebPrinter.Get_ReportName: WideString;
begin
  Result:=Self.ReportName;
end;

procedure TWebPrinter.Set_ReportName(const Value: WideString);
begin
  Self.ReportName:=Value;
end;

procedure TWebPrinter.printReport;
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,Self.AppName,Self.ReportName,Self.Params,self.URL);
  Report.printReport();
  FreeAndNil(Report);
end;

procedure TWebPrinter.designReport;
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,Self.AppName,Self.ReportName,Self.Params,Self.URL);
  Report.designReport;
  FreeAndNil(Report);
end;
procedure TWebPrinter.showReport;
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,Self.AppName,Self.ReportName,Self.Params,Self.URL);
  Report.showReport;
  FreeAndNil(Report);
end;
procedure TWebPrinter.ActiveFormDestroy(Sender: TObject);
begin
    FreeAndNil(self.frxPDFExport1);
    FreeAndNil(self.frxXLSExport1);
    FreeAndNil(self.frxRTFExport1);
    FreeAndNil(self.frxCSVExport1);
end;

function TWebPrinter.Get_Params: WideString;
begin
  Result:=Self.Params;
end;

procedure TWebPrinter.Set_Params(const Value: WideString);
begin
  Self.Params:=Value;
end;

initialization
  TActiveFormFactory.Create(
    ComServer,
    TActiveFormControl,
    TWebPrinter,
    Class_WebPrinter,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.
