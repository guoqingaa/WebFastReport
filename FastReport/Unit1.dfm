object Form1: TForm1
  Left = 115
  Top = 213
  Width = 962
  Height = 398
  Caption = 'FastReport'#24320#21457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 32
    Width = 112
    Height = 13
    Caption = 'FastReport'#27169#29256#25991#20214#65306
  end
  object Label2: TLabel
    Left = 40
    Top = 80
    Width = 112
    Height = 13
    Caption = 'FastReport'#25968#25454#25991#20214#65306
  end
  object Edit1: TEdit
    Left = 152
    Top = 24
    Width = 369
    Height = 21
    TabOrder = 0
    Text = 'D:\DpWorkSpace\WebFastReport\FastReport\test0.fr3'
    OnDblClick = Edit1DblClick
  end
  object Edit2: TEdit
    Left = 152
    Top = 72
    Width = 369
    Height = 21
    TabOrder = 1
    Text = 'D:\DpWorkSpace\WebFastReport\FastReport\data.xml'
    OnDblClick = Edit2DblClick
  end
  object Button1: TButton
    Left = 176
    Top = 128
    Width = 75
    Height = 25
    Caption = #35774#35745
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 264
    Top = 128
    Width = 75
    Height = 25
    Caption = #39044#35272
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 448
    Top = 128
    Width = 75
    Height = 25
    Caption = #26174#31034#25968#25454
    TabOrder = 4
    OnClick = Button3Click
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 168
    Width = 241
    Height = 121
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Visible = True
      end>
  end
  object DBGrid2: TDBGrid
    Left = 264
    Top = 168
    Width = 352
    Height = 120
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'code'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'name'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'state'
        Visible = True
      end>
  end
  object Button4: TButton
    Left = 360
    Top = 128
    Width = 75
    Height = 25
    Caption = #25171#21360
    TabOrder = 7
  end
  object Button5: TButton
    Left = 32
    Top = 128
    Width = 75
    Height = 25
    Caption = #30446#24405
    TabOrder = 8
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 24
    Top = 312
    Width = 129
    Height = 25
    Caption = #35831#27714#26381#21153#22120
    TabOrder = 9
    OnClick = Button6Click
  end
  object Memo1: TMemo
    Left = 632
    Top = 8
    Width = 225
    Height = 129
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 10
  end
  object Memo2: TMemo
    Left = 632
    Top = 144
    Width = 225
    Height = 129
    Lines.Strings = (
      'Memo2')
    ScrollBars = ssVertical
    TabOrder = 11
  end
  object Button7: TButton
    Left = 240
    Top = 312
    Width = 73
    Height = 25
    Caption = #39044#35272
    TabOrder = 12
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 328
    Top = 312
    Width = 81
    Height = 25
    Caption = #25171#21360
    TabOrder = 13
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 424
    Top = 312
    Width = 75
    Height = 25
    Caption = #35774#35745
    TabOrder = 14
    OnClick = Button9Click
  end
  object OpenDialog1: TOpenDialog
    Left = 728
    Top = 312
  end
  object OpenDialog2: TOpenDialog
    Left = 768
    Top = 312
  end
  object DataSource1: TDataSource
    Left = 648
    Top = 312
  end
  object DataSource2: TDataSource
    Left = 696
    Top = 312
  end
  object frxReport1: TfrxReport
    Version = '4.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = '???'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41098.493987199100000000
    ReportOptions.LastChange = 41110.013405219910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReport1GetValue
    Left = 800
    Top = 312
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData_ds1: TfrxMasterData
        Height = 37.795300000000000000
        Top = 73.000000000000000000
        Width = 793.701300000000000000
        RowCount = 0
        object ds1DOSE_ID: TfrxMemoView
          Left = 7.559060000000000000
          Top = 11.338590000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'DOSE_ID'
          DataSetName = 'ds1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[ds1."DOSE_ID"]')
          ParentFont = False
        end
        object ds1DOSE_CODE: TfrxMemoView
          Left = 215.433210000000000000
          Top = 11.338590000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'DOSE_CODE'
          DataSetName = 'ds1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[ds1."DOSE_CODE"]')
          ParentFont = False
        end
        object ds1DOSE_NAME: TfrxMemoView
          Left = 430.866420000000000000
          Top = 11.338590000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'DOSE_NAME'
          DataSetName = 'ds1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[ds1."DOSE_NAME"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 34.015770000000000000
        Top = 1089.000000000000000000
        Width = 793.701300000000000000
        object Page: TfrxMemoView
          Left = 582.047620000000000000
          Top = 7.559060000000040000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          Left = 691.653990000000000000
          Top = 7.559060000000040000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[TotalPages#]')
          ParentFont = False
        end
        object Page2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 7.559060000000040000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Page]')
          ParentFont = False
        end
        object TotalPages1: TfrxMemoView
          Left = 117.165430000000000000
          Top = 7.559060000000040000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[TotalPages]')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 268.346630000000000000
          Top = 7.559060000000040000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
        object Time: TfrxMemoView
          Left = 415.748300000000000000
          Top = 11.338590000000100000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Time]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 16.000000000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 283.464750000000000000
          Top = 7.559060000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #37814#12520#12291#29825#25424#57720)
          ParentFont = False
        end
      end
      object Subreport1: TfrxSubreport
        Left = 4.000000000000000000
        Top = 148.000000000000000000
        Width = 80.000000000000000000
        Height = 16.000000000000000000
        Page = frxReport1.Page3
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    PrintOptimized = False
    Outline = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Background = False
    Creator = 'FastReport (http://www.fast-report.com)'
    HTMLTags = True
    Left = 872
    Top = 16
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 872
    Top = 56
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    ShowProgress = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    Left = 912
    Top = 56
  end
  object frxCSVExport1: TfrxCSVExport
    UseFileCache = True
    ShowProgress = True
    Separator = ';'
    OEMCodepage = False
    Left = 912
    Top = 16
  end
end
